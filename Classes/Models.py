import numpy as np

class DiffusionModel:
    """Class defining a diffusion model"""

    def __init__(self, grid, phi, gamma, west_bc, east_bc):
        """Constructor"""
        self._grid = grid
        self._phi = phi
        self._gamma = gamma
        self._west_bc = west_bc
        self._east_bc = east_bc

    def add(self, coeffs):
        """Function to add diffusion terms to coefficient arrays"""

        # Calculate the west and east face diffusion flux terms for each face
        flux_w = - self._gamma*self._grid.Aw*(self._phi[1:-1]-self._phi[0:-2])/self._grid.dx_WP
        flux_e = - self._gamma*self._grid.Ae*(self._phi[2:]-self._phi[1:-1])/self._grid.dx_PE

        # Calculate the linearization coefficients
        coeffW = - self._gamma*self._grid.Aw/self._grid.dx_WP
        coeffE = - self._gamma*self._grid.Ae/self._grid.dx_PE
        coeffP = - coeffW - coeffE

        # Modify the linearization coefficients on the boundaries
        coeffP[0] += coeffW[0]*self._west_bc.coeff()
        coeffP[-1] += coeffE[-1]*self._east_bc.coeff()

        # Zero the boundary coefficients that are not used
        coeffW[0] = 0.0
        coeffE[-1] = 0.0

        # Calculate the net flux from each cell
        flux = flux_e - flux_w

        # Add to coefficient arrays
        coeffs.accumulate_aP(coeffP)
        coeffs.accumulate_aW(coeffW)
        coeffs.accumulate_aE(coeffE)
        coeffs.accumulate_rP(flux)

        # Return the modified coefficient array
        return coeffs

class SurfaceConvectionModel:
    """Class defining a surface convection model"""

    def __init__(self, grid, T, ho, To):
        """Constructor"""
        self._grid = grid
        self._T = T
        self._ho = ho
        self._To = To

    def add(self, coeffs):
        """Function to add surface convection terms to coefficient arrays"""

        # Calculate the source term
        source = self._ho*self._grid.Ao*(self._T[1:-1] - self._To)

        # Calculate the linearization coefficients
        coeffP = self._ho*self._grid.Ao

        # Add to coefficient arrays
        coeffs.accumulate_aP(coeffP)
        coeffs.accumulate_rP(source)

        return coeffs

class FirstOrderTransientModel:
    """Class defining a first order implicit transient model"""

    def __init__(self, grid, T, Told, rho, cp, dt):
        """Constructor"""
        self._grid = grid
        self._T = T
        self._Told = Told
        self._rho = rho
        self._cp = cp
        self._dt = dt

    def add(self, coeffs):
        """Function to add transient term to coefficient arrays"""

        # Calculate the transient term
        transient_term = self._rho*self._cp*self._grid._vol*(self._T[1:-1] - self._Told[1:-1]) * (1/self._dt)
        
        # Calculate the linearization coefficient
        coeffP = self._rho*self._cp*self._grid._vol*(1/self._dt)
        
        # Add to coefficient arrays
        coeffs.accumulate_aP(coeffP)
        coeffs.accumulate_rP(transient_term)

        return coeffs


class CrankNicholsonTransientModel:
    """Class defining a first order implicit transient model"""

    def __init__(self, grid, T, Told, Tolder, rho, cp, dt, k):
        """Constructor"""
        self._grid = grid
        self._T = T
        self._Told = Told
        self._Tolder = Tolder
        self._rho = rho
        self._cp = cp
        self._dt = dt
        self._k = k

    def add(self, coeffs):
        """Function to add transient term to coefficient arrays"""

        # Calculate the transient term
        transient_term = 2*self._rho*self._cp*self._grid._vol*(self._T[1:-1] - self._Told[1:-1]) * (1/self._dt)
        
        # Calculate previous diffusive fluxes
        Fw = -self._k*self._grid.Aw*(1/self._grid.dx_WP)*(self._Told[1:-1] - self._Told[0:-2])
        Fe = -self._k*self._grid.Ae*(1/self._grid.dx_PE)*(self._Told[2:] - self._Told[1:-1])
        flux = Fe - Fw
        
        # Calculate the linearization coefficient
        coeffP = 2*self._rho*self._cp*self._grid._vol*(1/self._dt) 
        
        # Add to coefficient arrays
        coeffs.accumulate_aP(coeffP)
        coeffs.accumulate_rP(transient_term)
        coeffs.accumulate_rP(flux)

        return coeffs

class SecondOrderTransientModel:
    """Class defining a second order implicit transient model"""

    def __init__(self, grid, T, Told, Tolder, rho, cp, dt):
        """Constructor"""
        self._grid = grid
        self._T = T
        self._Told = Told
        self._Tolder = Tolder
        self._rho = rho
        self._cp = cp
        self._dt = dt

    def add(self, coeffs):
        """Function to add transient term to coefficient arrays"""

        # Calculate the transient term
        transient_term = self._rho * self._cp * self._grid.vol * (
            1.5 * self._T[1:-1] - 2 * self._Told[1:-1] + 0.50 * self._Tolder[1:-1]) * (1/self._dt)
        
        # Calculate the linearization coefficient
        coeffP = 1.5*self._rho*self._cp*self._grid.vol*(1/self._dt)
        
        # Add to coefficient arrays
        coeffs.accumulate_aP(coeffP)
        coeffs.accumulate_rP(transient_term)

        return coeffs


class UpwindAdvectionModel:
    """Class defining an upwind advection model"""

    def __init__(self, grid, phi, Uhe, rho, cp, west_bc, east_bc, boundOutlet):
        """Constructor"""
        self._grid = grid
        self._phi = phi
        self._Uhe = Uhe
        self._rho = rho
        self._cp = cp
        self._west_bc = west_bc
        self._east_bc = east_bc
        self._alphae = np.zeros(self._grid.ncv+1)
        self._phie = np.zeros(self._grid.ncv+1)
        self._boundOutlet = boundOutlet

    def add(self, coeffs):
        """Function to add diffusion terms to coefficient arrays"""

        # Calculate the weighting factors
        for i in range(self._grid.ncv+1):
            if self._Uhe[i] >= 0:
                self._alphae[i] = 1
            else:
                self._alphae[i] = -1

        # Locate outlet
        outletPosition = -1 if self._Uhe[0] >= 0 else 0

        # Change alphae at outlet such that outlet temperature is set as specified temperature
        if self._boundOutlet:
            self._alphae[outletPosition] = -self._alphae[outletPosition]
        
        # Calculate the east integration point values (including both boundaries)
        self._phie = 0.5*(1 + self._alphae)*self._phi[:-1] + 0.5*(1 - self._alphae)*self._phi[1:]

        # Calculate the face mass fluxes
        mdote = self._rho*self._Uhe*self._grid.Af
        
        # Calculate the west and east face advection flux terms for each face
        flux_w = self._cp*mdote[:-1]*self._phie[:-1]
        flux_e = self._cp*mdote[1:]*self._phie[1:]
        
        # Calculate mass imbalance term
        imbalance = - self._cp*mdote[1:]*self._phi[1:-1] + self._cp*mdote[:-1]*self._phi[1:-1]
          
        # Calculate the linearization coefficients
        coeffW = -0.5*self._cp*mdote[:-1]*(1 + self._alphae[:-1])
        coeffE = 0.5*self._cp*mdote[1:]*(1 - self._alphae[1:])
        coeffP = - coeffW - coeffE

        # Modify the linearization coefficients on the boundaries
        coeffP[0] += coeffW[0]*self._west_bc.coeff()
        coeffP[-1] += coeffE[-1]*self._east_bc.coeff()

        # Zero the boundary coefficients that are not used
        coeffW[0] = 0.0
        coeffE[-1] = 0.0

        # Calculate the net flux from each cell
        flux = flux_e - flux_w

        # Add to coefficient arrays
        coeffs.accumulate_aP(coeffP)
        coeffs.accumulate_aW(coeffW)
        coeffs.accumulate_aE(coeffE)
        coeffs.accumulate_rP(flux)
        coeffs.accumulate_rP(imbalance)

        # Return the modified coefficient array
        return coeffs


class CentralDifferencingScheme:
    """Class defining a central difference advection model"""

    def __init__(self, grid, phi, Uhe, rho, cp, west_bc, east_bc, boundOutlet):
        """Constructor"""
        self._grid = grid
        self._phi = phi
        self._Uhe = Uhe
        self._rho = rho
        self._cp = cp
        self._west_bc = west_bc
        self._east_bc = east_bc
        self._alphae = np.zeros(self._grid.ncv+1)
        self._phie = np.zeros(self._grid.ncv+1)
        self._boundOutlet = boundOutlet

    def add(self, coeffs):
        """Function to add diffusion terms to coefficient arrays"""

        # Calculate the weighting factors
        for i in range(self._grid.ncv+1):
            if self._Uhe[i] >= 0:
                self._alphae[i] = 1
            else:
                self._alphae[i] = -1

        # Locate outlet
        outletPosition = -1 if self._Uhe[0] >= 0 else 0

        # Change alphae at outlet such that outlet temperature is set as specified temperature
        if self._boundOutlet:
            self._alphae[outletPosition] = -self._alphae[outletPosition]
        
        # Calculate the east integration point values (including both boundaries)
        self._phie[0] = 0.5*(1+self._alphae[0])*self._phi[0] + 0.5*(1-self._alphae[0])*self._phi[1]
        self._phie[-1] = 0.5*(1+self._alphae[-1])*self._phi[-2] + 0.5*(1-self._alphae[-1])*self._phi[-1]
        self._phie[1:-1] = (self._phi[1:-2] + self._phi[2:-1])/2

        # Calculate mass flow rate
        mdote = self._rho * self._Uhe * self._grid.Af

        # Calculate fluxes through faces
        flux_w = self._cp*mdote[:-1]*self._phie[:-1]
        flux_e = self._cp*mdote[1:]*self._phie[1:]

        # Calculate mass imbalance term
        imbalance = - self._cp*mdote[1:]*self._phi[1:-1] + self._cp*mdote[:-1]*self._phi[1:-1]

        # Calculate the linearization coefficients
        coeffW = -0.5*self._cp*mdote[:-1]*(1 + self._alphae[:-1])
        coeffE = 0.5*self._cp*mdote[1:]*(1 - self._alphae[1:])
        coeffP = - coeffW - coeffE

        # Modify the linearization coefficients on the boundaries
        coeffP[0] += coeffW[0]*self._west_bc.coeff()
        coeffP[-1] += coeffE[-1]*self._east_bc.coeff()

        # Zero the boundary coefficients that are not used
        coeffW[0] = 0.0
        coeffE[-1] = 0.0

        # Calculate the net flux from each cell
        flux = flux_e - flux_w

        # Add to coefficient arrays
        coeffs.accumulate_aP(coeffP)
        coeffs.accumulate_aW(coeffW)
        coeffs.accumulate_aE(coeffE)
        coeffs.accumulate_rP(flux)
        coeffs.accumulate_rP(imbalance)

        # Return the modified coefficient array
        return coeffs


class QuickScheme:
    """Class defining a central difference advection model"""

    def __init__(self, grid, phi, Uhe, rho, cp, west_bc, east_bc, boundOutlet):
        """Constructor"""
        self._grid = grid
        self._phi = phi
        self._Uhe = Uhe
        self._rho = rho
        self._cp = cp
        self._west_bc = west_bc
        self._east_bc = east_bc
        self._alphae = np.zeros(self._grid.ncv+1)
        self._phie = np.zeros(self._grid.ncv+1)
        self._boundOutlet = boundOutlet

    def add(self, coeffs):
        """Function to add diffusion terms to coefficient arrays"""

        # Calculate the weighting factors
        for i in range(self._grid.ncv+1):
            if self._Uhe[i] >= 0:
                self._alphae[i] = 1
            else:
                self._alphae[i] = -1

        outletPosition = -1 if self._Uhe[0] >= 0 else 0

        if self._boundOutlet:
            self._alphae[outletPosition] = -self._alphae[outletPosition]
            
        # Calculate face temperature values at both boundaries
        self._phie[0] = 0.5*(1+self._alphae[0])*self._phi[0] + 0.5*(1-self._alphae[0])*self._phi[1]
        self._phie[-1] = 0.5*(1+self._alphae[-1])*self._phi[-2] + 0.5*(1-self._alphae[-1])*self._phi[-1]

        # Store relevant grid locations 
        xW = self._grid.xP[:-3]
        xP = self._grid.xP[1:-2]
        xE = self._grid.xP[2:-1]
        xEE = self._grid.xP[3:]
        x = self._grid.xf[1:-1]

        # Calculate inner face temperature values according to QUICK scheme
        if self._Uhe[0] >= 0:
            self._phie[1:-1] = self._phi[:-3]*((x-xP)*(x-xE))/((xW-xP)*(xW-xE)) + \
            self._phi[1:-2]*((x-xW)*(x-xE))/((xP-xW)*(xP-xE)) + \
            self._phi[2:-1]*((x-xW)*(x-xP))/((xE-xW)*(xE-xP))

        else:
            self._phie[1:-1] = self._phi[3:]*((x-xE)*(x-xP))/((xEE-xE)*(xEE-xP)) + \
            self._phi[2:-1]*((x-xEE)*(x-xP))/((xE-xEE)*(xE-xP)) + \
            self._phi[1:-2]*((x-xEE)*(x-xE))/((xP-xEE)*(xP-xE))

        # Calculate mass flow rate
        mdote = self._rho * self._Uhe * self._grid.Af

        # Calculate fluxes through faces
        flux_w = self._cp*mdote[:-1]*self._phie[:-1]
        flux_e = self._cp*mdote[1:]*self._phie[1:]

        # Calculate mass imbalance term
        imbalance = - self._cp*mdote[1:]*self._phi[1:-1] + self._cp*mdote[:-1]*self._phi[1:-1]

        # Calculate the linearization coefficients
        coeffW = -0.5*self._cp*mdote[:-1]*(1 + self._alphae[:-1])
        coeffE = 0.5*self._cp*mdote[1:]*(1 - self._alphae[1:])
        coeffP = - coeffW - coeffE

        # Modify the linearization coefficients on the boundaries
        coeffP[0] += coeffW[0]*self._west_bc.coeff()
        coeffP[-1] += coeffE[-1]*self._east_bc.coeff()

        # Zero the boundary coefficients that are not used
        coeffW[0] = 0.0
        coeffE[-1] = 0.0

        # Calculate the net flux from each cell
        flux = flux_e - flux_w

        # Add to coefficient arrays
        coeffs.accumulate_aP(coeffP)
        coeffs.accumulate_aW(coeffW)
        coeffs.accumulate_aE(coeffE)
        coeffs.accumulate_rP(flux)
        coeffs.accumulate_rP(imbalance)

        # Return the modified coefficient array
        return coeffs

